﻿using System;
using System.Diagnostics;
using System.Net;
using Duality;
using Microsoft.Owin.Hosting;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;

namespace ClassLibrary1
{
    public class SignalRServer : Component, ICmpInitializable
    {
        private IDisposable _signalRServer;
        public int _port { get; set; } = 8080;

        public void StopServer()
        {
            if (_signalRServer != null)
                _signalRServer.Dispose();
        }

        public void OnInit(InitContext context)
        {
            if (context == InitContext.Activate && DualityApp.ExecContext == DualityApp.ExecutionContext.Game)
            {
                var networkDiscovery = new NetworkDiscovery(_port, "TestGame"); //Network discovery to get the ip adres of the server if one is found
                IPEndPoint ipEndPoint;
                if (networkDiscovery.LookForServer(out ipEndPoint))
                {
                    try
                    {
                        ConnectToServer(ipEndPoint).Wait();
                        Debug.WriteLine($"Connection established to {ipEndPoint}");
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("Could not find server");
                    }
                }
                else //No server was found so we create one
                {
                    Debug.WriteLine("Starting signalR server");
                    string url = $"http://*:{_port}"; //To test go to http://localhost:8080/signalr/hubs
                    networkDiscovery.Start();
                    _signalRServer = WebApp.Start<Startup>(url);
                }
            }
        }

        private async Task ConnectToServer(IPEndPoint ipEndPoint)
        {
            var hubConnection = new HubConnection($"http://{ipEndPoint}/");
            //var hubConnection = new HubConnection($"http://localhost:8080/");
            IHubProxy hubProxy = hubConnection.CreateHubProxy(nameof(MyHub));
            hubProxy.On<string, string>(nameof(MyHub.Send), (name, message) =>
            {
                Debug.WriteLine("Incoming data: {0} {1}", name, message);
            });
            ServicePointManager.DefaultConnectionLimit = 10;
            await hubConnection.Start();

        }

        public void OnShutdown(ShutdownContext context)
        {
            StopServer();
        }
    }

    public class MyHub : Hub
    {
        public void Send(string name, string message)
        {
            Clients.All.addMessage(name, message);
        }

        public override Task OnConnected()
        {
            Debug.WriteLine("Client connected: " + Context.ConnectionId);
            Send("Server", $"Client with id {Context.ConnectionId} has connected");
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            Debug.WriteLine("Client disconnected: " + Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }
    }
}
