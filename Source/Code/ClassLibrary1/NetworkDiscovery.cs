﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ClassLibrary1
{
    public class NetworkDiscovery
    {
        private int _port;
        private string _response;

        public NetworkDiscovery(int port, string response)
        {
            _port = port;
            _response = response;
        }

        public async void Start()
        {
            using (var Server = new UdpClient(_port))
            {
                var ResponseData = Encoding.ASCII.GetBytes(_response);

                while (true)
                {
                    var ClientEp = new IPEndPoint(IPAddress.Any, 0);
                    var ClientRequestData = Server.ReceiveAsync();
                    await ClientRequestData;
                    var ClientRequest = Encoding.ASCII.GetString(ClientRequestData.Result.Buffer);

                    Console.WriteLine("Recieved {0} from {1}, sending response", ClientRequest, ClientEp.Address.ToString());
                    Server.Send(ResponseData, ResponseData.Length, ClientRequestData.Result.RemoteEndPoint);
                }
            }
        }

        public bool LookForServer(out IPEndPoint serverEndPoint)
        {
            using (var Client = new UdpClient())
            {
                var RequestData = Encoding.ASCII.GetBytes(_response);
                serverEndPoint = new IPEndPoint(IPAddress.Any, 0);

                Client.Client.ReceiveTimeout = 2000;
                Client.EnableBroadcast = true;
                Debug.WriteLine("Sending request to server");
                Client.Send(RequestData, RequestData.Length, new IPEndPoint(IPAddress.Broadcast, _port));

                try
                {
                    var ServerResponseData = Client.Receive(ref serverEndPoint);
                    var ServerResponse = Encoding.ASCII.GetString(ServerResponseData);
                    Debug.WriteLine($"Recieved response from server at {serverEndPoint}");
                    return true;
                }
                catch (SocketException ex)
                {
                    return false;
                }
            }
        }
    }
}