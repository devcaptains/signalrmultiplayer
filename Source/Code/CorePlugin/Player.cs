﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Components.Physics;
using Duality.Input;
using SnowyPeak.Duality.Plugins.YAUI.Controls;

namespace Duality_
{
    [RequiredComponent(typeof(RigidBody))]
    public class Player : Component, ICmpUpdatable
    {
        public RigidBody RigidBody { get; set; }
        public float foo { get; set; }
        public void OnUpdate()
        {
            var rigidBody = GameObj.GetComponent<RigidBody>();
            if (DualityApp.Keyboard[Key.Left])
            {
                rigidBody.ApplyLocalForce(-0.001f * rigidBody.Inertia);
            }
            else if (DualityApp.Keyboard[Key.Right])
            {
                rigidBody.ApplyLocalForce(0.001f * rigidBody.Inertia);
            }
            else
            {
                rigidBody.AngularVelocity -= rigidBody.AngularVelocity * 0.1f * Time.TimeMult;
            }

            if (DualityApp.Keyboard[Key.Up])
            {
                rigidBody.ApplyLocalForce(Vector2.UnitY * -0.2f * rigidBody.Mass);
            }
            else if (DualityApp.Keyboard[Key.Down])
            {
                rigidBody.ApplyLocalForce(Vector2.UnitY * 0.2f * rigidBody.Mass);
            }
        }
    }
}
